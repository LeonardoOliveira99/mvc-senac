@extends('layouts.index')
@section('title', 'Feriados')

@section('content')

<table class="table">
  <thead>
    <tr>
      <th scope="col">ID</th>
      <th scope="col">Feriado</th>
      <th scope="col">Data</th>
    </tr>
  </thead>
  <tbody>

    @foreach ($feriados as $key => $feriado)
        <tr>
        <th scope="row">{{ $feriado['id']}}</th>
        <td>{{ $feriado['nome']}}</td>
        <td>{{ $feriado['data']}}</td>
        </tr>
    @endforeach
  </tbody>
</table>
@endsection
